// SPDX-License-Identifier: MIT

using imperfect.log.stream.test.util;
using log4net;
using NUnit.Framework;
using System;
using System.IO;
using System.Linq;
using System.Text;

namespace imperfect.log.stream.test
{
	public class BinaryLoggedStreamTest
	{
		private const string ReadPrefix = "Read: ";
		private const string WrittenPrefix = "Written: ";

		private MemoryStreamLog4Net? simpleLog4Net = null;

		[SetUp]
		public void Init()
		{
			simpleLog4Net = new MemoryStreamLog4Net("%message");
		}

		[TearDown]
		public void Cleanup()
		{
			simpleLog4Net?.Dispose();
		}

		[Test]
		public void CanLogReadAndWriteMessages()
		{
			// Given
			ILog logger = LogManager.GetLogger(typeof(LoggedStreamTest));

			MemoryStream memoryStream = new MemoryStream();
			BinaryLoggedStream loggedStream = new BinaryLoggedStream(memoryStream, logger)
			{
				ReadPrefix = ReadPrefix,
				WritePrefix = WrittenPrefix
			};
			var message = new byte[] { 0x00, 0x01, 0x0f1, 0x00 };

			// When we write something then read it back
			loggedStream.Write(message);
			loggedStream.Position = 0; // Start reading from the beginning.
			using var streamReader = new StreamReader(loggedStream, Encoding.UTF8);
			var lineReadBack = streamReader.ReadLine();

			// Then
			simpleLog4Net?.ResetStreamPosition(); // Start reading from the beginning.
			using var memoryStreamReader = new StreamReader(simpleLog4Net!.Stream, Encoding.UTF8);
			var writtenLine = memoryStreamReader.ReadLine(); // Written line
			var readLine = memoryStreamReader.ReadLine() ?? ""; // Read line

			Assert.AreEqual($"{WrittenPrefix}{BitConverter.ToString(message)}", writtenLine);
			Assert.AreEqual($"{ReadPrefix}{BitConverter.ToString(message)}", readLine);
		}

		[Test]
		public void OrignalMessageDoesNotChange()
		{
			// Given
			ILog logger = LogManager.GetLogger(typeof(LoggedStreamTest));

			var memoryStream = new MemoryStream();
			BinaryLoggedStream loggedStream = new BinaryLoggedStream(memoryStream, logger)
			{
				ReadPrefix = ReadPrefix,
				WritePrefix = WrittenPrefix
			};
			var message = new byte[] { 0x00, 0x01, 0x0f1, 0x00 };

			// When
			loggedStream.Write(message);
			
			// Then
			loggedStream.Position = 0; // Start reading from the beginning.
			var readBytes = ReadBytes(loggedStream);

			Assert.AreEqual(message, readBytes);
		}

		byte[] ReadBytes(Stream stream)
		{
			const int bufferSize = 4092;
			byte[] readBuffer = new byte[bufferSize];
			int readCount = stream.Read(readBuffer, 0, bufferSize);
			return readBuffer.Take(readCount).ToArray();
		}
	}
}
