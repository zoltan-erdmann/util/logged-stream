// SPDX-License-Identifier: MIT

using System;
using System.IO;
using log4net;

namespace imperfect.log.stream
{
	public class BinaryLoggedStream : Stream
	{
		private readonly Stream stream;
		private readonly ILog logger;

		public string ReadPrefix { get; set; } = "";
		public string WritePrefix { get; set; } = "";
		public override bool CanRead => stream.CanRead;
		public override bool CanSeek => stream.CanSeek;
		public override bool CanWrite => stream.CanWrite;
		public override long Length => stream.Length;
		public override long Position
		{
			get => stream.Position;
			set => stream.Position = value;
		}
		public override int ReadTimeout
		{
			get => stream.ReadTimeout;
			set => stream.ReadTimeout = value;
		}

		public BinaryLoggedStream(Stream stream, ILog logger)
		{
			this.stream = stream;
			this.logger = logger;
		}
		
		public override void Flush()
		{
			stream.Flush();
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			int readCount = stream.Read(buffer, offset, count);
			if (readCount > 0)
			{
				logger.Info($"{ReadPrefix}{BitConverter.ToString(buffer, offset, readCount)}");
			}
			return readCount;
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			return stream.Seek(offset, origin);
		}

		public override void SetLength(long value)
		{
			stream.SetLength(value);
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			stream.Write(buffer, offset, count);
			logger.Info($"{WritePrefix}{BitConverter.ToString(buffer, offset, count)}");
		}
	}
}
