# Stream logger

This package contains two transparent stream implementations.
`LoggedStream` and `BinaryLoggedStream`.

You need to specifí the underlying stream and the logger in the constructors.
For the `LoggedStream`, you also have to set the encoding.

There are also two optional parameters (ReadPrefix, WritePrefix) that can be specified.

For example:
```csharp
var loggerName = $"{typeof(LoggedStream)}.COM1";
ILog logger = LogManager.GetLogger(loggerName);
var loggedStream = new BinaryLoggedStream(stream, logger)
{
	ReadPrefix = "Received: ",
	WritePrefix = "Sent: "
};
```
