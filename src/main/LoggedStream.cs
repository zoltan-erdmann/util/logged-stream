// SPDX-License-Identifier: MIT

using System.IO;
using System.Text;
using log4net;

namespace imperfect.log.stream
{
	public class LoggedStream : Stream
	{
		private readonly Stream stream;
		private readonly ILog logger;
		private readonly Encoding encoding;

		public string ReadPrefix { get; set; } = "";
		public string WritePrefix { get; set; } = "";
		public override bool CanRead => stream.CanRead;
		public override bool CanSeek => stream.CanSeek;
		public override bool CanWrite => stream.CanWrite;
		public override long Length => stream.Length;
		public override long Position
		{
			get => stream.Position;
			set => stream.Position = value;
		}
		public override int ReadTimeout
		{
			get => stream.ReadTimeout;
			set => stream.ReadTimeout = value;
		}

		public LoggedStream(Stream stream, ILog logger)
			: this(stream, logger, Encoding.UTF8)
		{
			// Intentional SKIP.
		}

		public LoggedStream(Stream stream, ILog logger, Encoding encoding)
		{
			this.stream = stream;
			this.logger = logger;
			this.encoding = encoding;
		}

		public override void Flush()
		{
			stream.Flush();
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			int readCount = stream.Read(buffer, offset, count);
			if (readCount > 0)
			{
				logger.Info($"{ReadPrefix}{encoding.GetString(buffer, offset, readCount)}");
			}
			return readCount;
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			return stream.Seek(offset, origin);
		}

		public override void SetLength(long value)
		{
			stream.SetLength(value);
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			stream.Write(buffer, offset, count);
			logger.Info($"{WritePrefix}{encoding.GetString(buffer, offset, count)}");
		}
	}
}
