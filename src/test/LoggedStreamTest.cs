// SPDX-License-Identifier: MIT

using imperfect.log.stream.test.util;
using log4net;
using NUnit.Framework;
using System.IO;
using System.Text;

namespace imperfect.log.stream.test
{
	public class LoggedStreamTest
	{
		private const string ReadPrefix = "Read: ";
		private const string WrittenPrefix = "Written: ";

		private MemoryStreamLog4Net? simpleLog4Net = null;

		[SetUp]
		public void Init()
		{
			simpleLog4Net = new MemoryStreamLog4Net("%message");
		}

		[TearDown]
		public void Cleanup()
		{
			simpleLog4Net?.Dispose();
		}

		[Test]
		public void CanLogReadAndWriteMessages()
		{
			// Given
			ILog logger = LogManager.GetLogger(typeof(LoggedStreamTest));

			MemoryStream memoryStream = new MemoryStream();
			LoggedStream loggedStream = new LoggedStream(memoryStream, logger)
			{
				ReadPrefix = ReadPrefix,
				WritePrefix = WrittenPrefix
			};
			var message = "Some text.";

			// When we write something then read it back
			loggedStream.Write(Encoding.UTF8.GetBytes(message));
			loggedStream.Position = 0; // Start reading from the beginning.
			using var streamReader = new StreamReader(loggedStream, Encoding.UTF8);
			var lineReadBack = streamReader.ReadLine();

			// Then
			simpleLog4Net?.ResetStreamPosition(); // Start reading from the beginning.
			using var memoryStreamReader = new StreamReader(simpleLog4Net!.Stream, Encoding.UTF8);
			var writtenLine = memoryStreamReader.ReadLine(); // Written line
			var readLine = memoryStreamReader.ReadLine() ?? ""; // Read line

			Assert.AreEqual($"{WrittenPrefix}{message}", writtenLine);
			Assert.AreEqual($"{ReadPrefix}{message}", readLine);
		}

		[Test]
		public void OrignalMessageDoesNotChange()
		{
			// Given
			ILog logger = LogManager.GetLogger(typeof(LoggedStreamTest));

			var memoryStream = new MemoryStream();
			LoggedStream loggedStream = new LoggedStream(memoryStream, logger)
			{
				ReadPrefix = ReadPrefix,
				WritePrefix = WrittenPrefix
			};
			var message = "Some text.";

			// When
			loggedStream.Write(Encoding.UTF8.GetBytes(message));

			// Then
			loggedStream.Position = 0; // Start reading from the beginning.
			using var memoryStreamReader = new StreamReader(loggedStream, Encoding.UTF8);
			var line = memoryStreamReader.ReadLine() ?? "";

			Assert.AreEqual(message, line);
		}
	}
}
