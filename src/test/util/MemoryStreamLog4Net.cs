// SPDX-License-Identifier: MIT

using log4net;
using log4net.Core;
using log4net.Layout;
using log4net.Repository.Hierarchy;
using System;
using System.IO;

namespace imperfect.log.stream.test.util
{
	internal sealed class MemoryStreamLog4Net : IDisposable
	{
		public Stream Stream { get; } = new MemoryStream();

		public MemoryStreamLog4Net(string pattern)
		{
			Hierarchy hierarchy = (Hierarchy)LogManager.GetRepository();

			var patternLayout = CreatePatternLayout(pattern);
			var memoryStreamAppender = CreateMemoryStreamAppender(patternLayout);
			hierarchy.Root.AddAppender(memoryStreamAppender);
			hierarchy.Root.Level = Level.All;
			hierarchy.Configured = true;
		}

		public void ResetStreamPosition()
		{
			Stream.Position = 0; // Start reading from the beginning.
		}

		private MemoryStreamAppender CreateMemoryStreamAppender(PatternLayout patternLayout)
		{
			var memoryAppender = new MemoryStreamAppender(Stream);
			memoryAppender.Layout = patternLayout;
			memoryAppender.ActivateOptions();
			return memoryAppender;
		}

		private PatternLayout CreatePatternLayout(string pattern)
		{
			var patternLayout = new PatternLayout();
			patternLayout.ConversionPattern = pattern;
			patternLayout.ActivateOptions();
			return patternLayout;
		}

		public void Dispose()
		{
			LogManager.ResetConfiguration();
			Hierarchy hierarchy = (Hierarchy)LogManager.GetRepository();
			hierarchy.Root.RemoveAllAppenders();
		}
	}
}
